<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $fillable =[
        'temp_user_id','name','email','address','country','city','postal_code','phone'
    ];
}
