<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class NotifyCustomerForPlacedOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {

        Mail::send('mail', $event->shipping_info->email, function($message) use ($event) {
            $message->to($event->shipping_info->email, 'name of customer')->subject
            ('Order Conformed By Medifirmspharma');
            $message->from('shubhamget@gmail.com','Medifirmspharma');
        });
        echo "Basic Email Sent. Check your inbox.";
    }
}
