<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountByQuantity extends Model
{
    protected $fillable =[
        'discount_by_quantities_id','data','discount_type'
    ];
}
